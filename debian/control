Source: anonip
Section: admin
Priority: optional
Maintainer: Alexander Reichle-Schmehl <tolimar@debian.org>
Build-Depends:
 debhelper-compat (= 13),
 dh-python,
 dh-sequence-python3,
 python3,
 python3-pytest,
 python3-pytest-cov,
 python3-setuptools,
 help2man,
Standards-Version: 4.6.1
Homepage: https://github.com/DigitaleGesellschaft/Anonip
Vcs-Browser: https://salsa.debian.org/debian/anonip
Vcs-Git: https://salsa.debian.org/debian/anonip.git
Rules-Requires-Root: no

Package: anonip
Architecture: all
Depends: ${python3:Depends}, ${misc:Depends}
Suggests: python-anonip-doc
Description: Anonymize IP-addresses in log-files
 Anonip masks the last bits of IPv4 and IPv6 addresses. That way most of the
 relevant information is preserved, while the IP-address does not match a
 particular individuum anymore.
 .
 It can be either used to anonymise existing log files, or directly by a service
 if it supports loging to shell pipes. Configuring the Apache httpd or nginx is
 very easy, so that unmasked IP  addresses are never  written to any file.
 .
 With the help of cat, it's also possible to rewrite existing log files.
